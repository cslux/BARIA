/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * BARIA - Backup And Restore Installed Apps
 * Copyright (C) 2016  vishnu@easwareapps.com
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.baria.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.easwareapps.baria.utils.BariaPref;
import com.easwareapps.baria.utils.PInfo;
import com.easwareapps.baria.R;
import com.easwareapps.baria.adapter.SavedAppsAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;



public class SavedAppsFragment extends Fragment {

    ArrayList<PInfo> res;
    RecyclerView apps;
    SavedAppsAdapter savedAppsAdapter;
    static int iconSize = -1;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_saved_apps, container, false);
        apps = (RecyclerView) rootView.findViewById(R.id.appsList);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        apps.setLayoutManager(llm);
        getIconSize();
        if(checkStoragePermission(0)) {
            BariaPref pref = BariaPref.getInstance(getActivity());
            String path = pref.getBackupDir();
            if((!new File(path).exists()) && !new File(path).isDirectory()) {
                if(!new File(path).mkdirs()){
                    return rootView;
                }
            }
            refreshApps();
        }

        return rootView;
    }


    @TargetApi(23)
    private boolean checkStoragePermission(int requestCode) {

        String permissions[] = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if(isPermissionGranted(permissions[0])) {
            return true;
        }
        getActivity().requestPermissions(permissions, requestCode);
        return false;

    }

    @TargetApi(23)
    private boolean isPermissionGranted(String permission){
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        int hasStoragePermission = getActivity().checkSelfPermission(permission);
        if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
            return false;

        }else {
            return true;
        }
    }



    public void refreshApps() {
        if(checkStoragePermission(0)) {
            new AppsLoader().execute();
        }
    }

    class AppsLoader extends AsyncTask <Void, Void, Void>{



        @Override
        protected Void doInBackground(Void... voids) {
            getApps();
            return null;
        }

        void getApps(){
            res = new ArrayList<>();
            BariaPref pref = BariaPref.getInstance(getActivity());
            String  path = pref.getBackupDir();
            File directory = new File(path);
            File[] files = directory.listFiles();
            PackageManager pm = getActivity().getPackageManager();
            for (int i = 0; i < files.length; i++)
            {
                if (!files[i].getAbsolutePath().endsWith(".apk")) {
                    continue;
                }
                PackageInfo p = pm.getPackageArchiveInfo(files[i].getAbsolutePath(), 0);

                try {
                    p.applicationInfo.sourceDir = files[i].getAbsolutePath();
                    p.applicationInfo.publicSourceDir = files[i].getAbsolutePath();
                    PInfo newInfo = new PInfo();
                    newInfo.appname = p.applicationInfo.loadLabel(getActivity().getPackageManager()).toString();
                    newInfo.pname = p.packageName;
                    newInfo.versionName = p.versionName;
                    newInfo.versionCode = p.versionCode;
                    newInfo.apk = files[i].getAbsolutePath();
                    newInfo.icon = p.applicationInfo.loadIcon(getActivity().getPackageManager());
                    res.add(newInfo);
                }catch (Exception e) {

                }
            }
            Collections.sort(res, new Comparator<PInfo>() {

                public int compare(PInfo p1, PInfo p2) {
                    return p1.appname.compareTo(p2.appname);
                }
            });
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            savedAppsAdapter = SavedAppsAdapter.getInstance(res,
                    getActivity(), getIconSize(), apps, getActivity());

            apps.setAdapter(savedAppsAdapter);





        }
    }







    private int getIconSize(){
        if(iconSize == -1) {
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            final int width = (dm.widthPixels > dm.heightPixels) ? dm.heightPixels:dm.widthPixels;
            iconSize = width / 5;
        }
        return iconSize;
    }



    public void installApps() {
        if(savedAppsAdapter != null) {
            if(checkStoragePermission(12)) {
                savedAppsAdapter.installAPK(null);
            }
        }
    }



    public void closeActionMode() {
        if (savedAppsAdapter!=null)
        savedAppsAdapter.closeActionMode();
    }



}
