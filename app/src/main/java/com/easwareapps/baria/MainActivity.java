/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * BARIA - Backup And Restore Installed Apps
 * Copyright (C) 2016  vishnu@easwareapps.com
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 package com.easwareapps.baria;

import android.*;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


import com.easwareapps.baria.fragments.InstalledAppsFragment;
import com.easwareapps.baria.fragments.SavedAppsFragment;
import com.easwareapps.baria.utils.BariaPref;


public class MainActivity extends AppCompatActivity {


    private static final int SETTINGS_REQ = 1000;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public static boolean showSystemApp = false;


    InstalledAppsFragment installedAppsFragment = null;
    SavedAppsFragment savedAppsFragment = null;
    private int theme;
    private String oldBackupDir = "";






    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        BariaPref pref = BariaPref.getInstance(getApplicationContext());
        theme = pref.getTheme(false);
        oldBackupDir = pref.getBackupDir();
        showSystemApp = pref.getPref(BariaPref.SHOW_SYSTEM_APP, false);
        setTheme(theme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(toolbar != null)
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                try {
                    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
                    if (position == 0) {
                        fab.setImageResource(R.mipmap.ic_copy);
                        savedAppsFragment.closeActionMode();
                    } else if (position == 1) {
                        installedAppsFragment.closeActionMode();
                        fab.setImageResource(R.mipmap.ic_install);
                    }
                } catch (Exception e) {

                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });



        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if (installedAppsFragment != null && mViewPager.getCurrentItem() == 0) {
                        installedAppsFragment.saveApps();
                    } else if (savedAppsFragment != null && mViewPager.getCurrentItem() == 1) {
                        savedAppsFragment.installApps();
                    }

            }
        });
        fab.setVisibility(View.GONE);







    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem mi = menu.findItem(R.id.action_toggle_system_app_visibility);
        changeMenuText(mi);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settings = new Intent(this, SettingsActivity.class);
            startActivityForResult(settings, SETTINGS_REQ);
            return true;
        }

        else if (id == R.id.action_toggle_system_app_visibility) {
            showSystemApp = !showSystemApp;
            if(installedAppsFragment != null) {
                installedAppsFragment.reloadApps();
            }
            changeMenuText(item);

        }
        else if (id == R.id.action_refresh) {
            if (mViewPager.getCurrentItem() == 0) {
                installedAppsFragment.reloadApps();
            } else if (mViewPager.getCurrentItem() == 1) {
                savedAppsFragment.refreshApps();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SETTINGS_REQ) {
            if(theme != BariaPref.getInstance(getApplicationContext()).getTheme(false)) {
                MainActivity.this.recreate();
            }
            BariaPref pref = BariaPref.getInstance(getApplicationContext());
            if (!oldBackupDir.equals(pref.getBackupDir())) {
               if (savedAppsFragment != null) {
                  savedAppsFragment.refreshApps();
               }
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void changeMenuText(MenuItem mi) {
        if(showSystemApp) {
            mi.setTitle(R.string.hide_system_apps);
        }else {
            mi.setTitle(R.string.show_system_apps);
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    if (installedAppsFragment == null)
                        installedAppsFragment = new InstalledAppsFragment();

                    return installedAppsFragment;
                case 1:
                    if (savedAppsFragment == null) savedAppsFragment = new SavedAppsFragment();

                    return savedAppsFragment;
            }

            return null;


        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.backup);
                case 1:
                    return getResources().getString(R.string.restore);
            }
            return null;
        }


    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (isPermissionGranted(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            if (requestCode == 11) {

                installedAppsFragment.saveApps();
            }

            else if (requestCode == 0) {
                savedAppsFragment.refreshApps();
            } else if(requestCode ==12) {
                if (savedAppsFragment != null) {
                    savedAppsFragment.installApps();
                }
            }
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }else {
            Snackbar.make(findViewById(R.id.main_content),
                    getResources().getString(R.string.allow_storage_permission),
                    Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.allow), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            try {
                                //Open the specific App Info page:
                                Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                intent.setData(Uri.parse("package:" + getPackageName()));
                                startActivity(intent);

                            } catch ( ActivityNotFoundException e ) {
                                Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                                startActivity(intent);

                            }
                        }
                    }).show();
        }
    }

    @TargetApi(23)
    private boolean isPermissionGranted(String permission){
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }else {
            int hasStoragePermission = checkSelfPermission(permission);
            if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
                return false;

            }
        }
        return true;
    }
}
