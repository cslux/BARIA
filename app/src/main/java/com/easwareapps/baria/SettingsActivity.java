/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * BARIA - Backup And Restore Installed Apps
 * Copyright (C) 2016  vishnu@easwareapps.com
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 package com.easwareapps.baria;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.SwitchPreference;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.easwareapps.baria.utils.BariaPref;


/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity
        implements Preference.OnPreferenceChangeListener{

    private static final int CHOOSE_DIR = 78901;
    private Preference prefBackup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        BariaPref pref = BariaPref.getInstance(getApplicationContext());
        int theme = pref.getTheme(false);
        setTheme(theme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_back);
        setSupportActionBar(toolbar);
        if(toolbar != null) {
            toolbar.setTitle(R.string.settings);
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }

        addPreferencesFromResource(R.xml.pref_general);
        Preference prefRefer = findPreference("refer");
        prefRefer.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                refer(null);
                return false;
            }
        });

        Preference prefRate = findPreference("rate");
        prefRate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                gotoPlayStore();
                return false;
            }
        });

        prefBackup = findPreference("backup_directory");
        prefBackup.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent dirChooser = new Intent(getApplicationContext(),
                        DirectoryChooserActivity.class);
                startActivityForResult(dirChooser, CHOOSE_DIR);
                return false;
            }
        });

        String dir = pref.getBackupDir();
        prefBackup.setSummary(dir);

        SwitchPreference darkTheme = (SwitchPreference) findPreference(BariaPref.DARK_THEME);
        darkTheme.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                BariaPref pref = BariaPref.getInstance(getApplicationContext());
                pref.setPref(BariaPref.DARK_THEME, (boolean)value);
                changeTheme();
                return true;
            }
        });

        SwitchPreference showSystemApp = (SwitchPreference)
                findPreference(BariaPref.SHOW_SYSTEM_APP);
        showSystemApp.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                BariaPref pref = BariaPref.getInstance(getApplicationContext());
                pref.setPref(BariaPref.SHOW_SYSTEM_APP, (boolean)value);
                return true;
            }
        });


    }

    private void changeTheme() {

        SettingsActivity.this.recreate();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CHOOSE_DIR) {
            if(resultCode != -1 && data != null) {
                String dir = data.getStringExtra("directory");
                if(!dir.equals("")) {
                    BariaPref pref = BariaPref.getInstance(getApplicationContext());
                    pref.setPref(BariaPref.BACKUP_DIR, dir);
                    prefBackup.setSummary(dir);
                    //Toast.makeText(getApplicationContext(), dir, Toast.LENGTH_LONG).show();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void gotoPlayStore() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.
                parse("https://play.google.com/store/apps/details?" +
                        "id=com.easwareapps.baria"));
        startActivity(intent);
    }


    @Override
    public boolean onPreferenceChange(Preference preference, Object o) {
        if (preference == findPreference(BariaPref.DARK_THEME)) {
            Toast.makeText(getApplicationContext(), "DARK THEME", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    public void refer (View v) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        share.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        share.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        share.setType("text/html");
        share.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.baria));
        share.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.baria_download, "http://baria.easwareapps.com"));
        startActivity(share);

    }
}
